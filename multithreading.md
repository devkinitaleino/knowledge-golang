# Многопоточность в Go

## Урок 1. Что такое многопоточность и ее реализация в языке Go
### Блок 1. Многопоточность
- Что такое многопоточность? 
- Конкурентное выполнение и параллельное выполнение

![img_2.png](img_2.png)

- Полезные ссылки:
    - https://vk.com/wall-84793390_14122
    - https://www.baeldung.com/cs/concurrency-vs-parallelism

Результат прохождения блока 1: Студент понимает концепцию многопоточности, конкурентного и параллельного выполнения программы. Эти абстрактные знания помогут лучше понять частную реализацию в языке Go.

### Блок 2. Реализация многопоточности в Go
- Горутины: концепция, синтаксис
- Важность потокобезопасности, состояние гонки:
    - Определение
    - go run -race main.go
    - Ссылка на то, что решения будут изложены ниже
- Горутины и потоки
- Планировщик Go
- Полезные ссылки:
    - https://proglib.io/p/gorutiny-chto-takoe-i-kak-rabotayut-2022-07-31
    - https://go.dev/tour/concurrency/1
    - https://www.educative.io/answers/what-are-race-conditions-in-golang
    - https://www.ardanlabs.com/blog/2014/01/concurrency-goroutines-and-gomaxprocs.html
    - https://habr.com/ru/articles/489862/

Результат прохождения блока 2: Студент понимает, что такое горутины и как их использовать, осознает возможные проблемы при неправильном использовании
горутин. Студент знает, чем горутины отличаются от потоков операционной системы, имеет представление, как работает планировщик (логические процессоры, локальная и глобальная очереди выполнения, переключения контекста и т.д.). Такие базовые знания
о многопоточности в Go будут применимы при написании практически любого функционала, который должен выполняться конкурентно или параллельно.
### Блок 3. Применение многопоточности в Go
- Учебные примеры, иллюстрирующие поведение горутин
```go
// Пример 1. Иллюстрация запуска горутины

package main

import (
	"fmt"
	"time"
)

func say(s string) {
	for i := 0; i < 5; i++ {
		time.Sleep(100 * time.Millisecond)
		fmt.Println(s)
	}
}

func main() {
	go say("world")
	say("hello")
}
```

```go
// Пример 2. Простейший пример состояния гонки

package main

import (
  "time"
)

var count int

func race() {
  count++
}

func main() {
  go race()
  go race()
  time.Sleep(1 * time.Second)
}
```
- Упрощенные примеры из реальной практики
```go
// Пример 1. Использование горутины для задачи, которая должна выполняться каждый раз в определенный интервал времени

package main

import (
  "time"
)

func main()  {
  // ...
  go cronTask(100)
  // ...
} 

func cronTask(interval int) {
  t := time.NewTicker(time.Second * interval)
  for range t.C {
	  // execution
  }
}
```

```go
// Пример 2. Обработка тяжелых вычислений в фоне

package core

import (
    "time"
)

type Repo interface {
	Create(object any) error
	Update(object any)
}

type Core struct {
	repo Repo
}

func (core *Core) Upload(object any) { 
	// ... 
	if err := core.repo.Create(object); err != nil { 
		// handle error (wrap or something) 
		return 
	} 
	// ... 
	go func() {
		// Отправляем задачу в фон, а пользователю быстро возвращаем ответ
		// Подходит не всем системам, но в некоторых ситуациях, можно сказать, необходимо.
		updatedObject := core.complicatedCalculation(object)
		core.repo.Update(updatedObject)
	}()
	return
}

func (core *Core) complicatedCalculation (object any) any { 
	// calculation
	return object
}
```
### Блок 4. Практические задания
```go
// Какой способ запуска горутин будет синтаксически корректным?

a)

    go someWork()

b)

    func() go {
        quad := numberQuad(2)
        result := quad * 3
        fmt.Println(result)
    }()

c)

    go quad := numberQuad(2)

d)

    work := func() {
        go someWork()
    }
    go work()

// Ответ: a,d
```

```go
/*
    Вы пришли на новый проект. 
    Разработчик, который уволился прямо перед вашим выходом, не успел доделать свою задачу. 
    Аналитик говорит, что некоторая операция должна выполняться раз в сутки, 
    а сейчас она выполняется только один раз при запуске сервиса.
    Как вы поступите?
 */

package worker 

// Вызывается при запуске сервиса
func trigger() {
    go worker()
}

func worker() {
    executeTask()
}

func executeTask() {
  // execution
}




Решение:

package worker

// Вызывается при запуске сервиса
func trigger() {
  go worker()
}

func worker() {
  t := time.NewTicker(time.Hour * 24)
  for range t.C {
    executeTask()
  }
}

func executeTask() {
  // execution
}
```

## Урок 2. Общение между горутинами, каналы
### Блок 1. Каналы
- Синтаксис каналов: создание, запись, чтение, закрытие
- Буферизированные и небуферизированные каналы
- Длина и емкость канала
- Ссылки:
    - https://habr.com/ru/articles/490336/
    - https://go.dev/tour/concurrency/2
    - https://go.dev/tour/concurrency/3

Результат прохождения блока 1: Студент знает, что такое каналы в Go, умеет проводить базовые операции с ними, 
знаком с их свойствами и разновидностью. Эти знания являются базовыми для перехода к изучению применения каналов.

### Блок 2. Использования каналов и особенности работы с ними.
- Каналы как инструмент коммуникации между горутинами
- deadlock
- Цикл for
- Оператор select
- Однонаправленные каналы
- Ссылки:
    - https://habr.com/ru/articles/490336
    - https://ru.wikipedia.org/wiki/%D0%92%D0%B7%D0%B0%D0%B8%D0%BC%D0%BD%D0%B0%D1%8F_%D0%B1%D0%BB%D0%BE%D0%BA%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0
    - https://www.atatus.com/blog/go-channels-overview/
    - https://go.dev/tour/concurrency/4
    - https://go.dev/tour/concurrency/5
    - https://go.dev/tour/concurrency/6

Результат прохождения блока 2: Студент знает, как применять каналы. Знаком с различными конструкциями для работы с каналами. Студент понимает возможные проблемы, которые могут
быть вызваны неправильным использованием каналов. На практике умение работать с каналами сильно расширяет возможности разработчика при написании многопоточных приложений, так как одно из важнейших применений каналов - общение между горутинами.

### Блок 3. Примеры использования каналов
```go
// Пример 1. Простейший пример использования каналов

package main

import "fmt"

func greet(c chan string) {
	fmt.Println("Hello, " + <-c + "!")
}

func main() {
	fmt.Println("main goroutine started")
	c := make(chan string)

	go greet(c)

	// Установи свое имя
	c <- "Anon"
	fmt.Println("main goroutine stopped")
}
```

```go
// Пример 2. Deadlock

package main

import "fmt"

// Возникновения deadlock в результате взаимоблокировки

// Output example
//
// Try to receive message
// Try to receive message
// fatal error: all goroutines are asleep - deadlock!

func main() {
	messages := make(chan string)

	go func() {
		fmt.Println("Try to receive message")
		<-messages                            // Blocking
		fmt.Println("Receive message")
	}()

	fmt.Println("Try to receive message") 
	<-messages                            // Blocking
	fmt.Println("Receive message")       

}
```

### Блок 4. Устройство каналов
- Структура канала hchan
- Кольцевые очереди
- Механика чтения и записи в канал
- Структура sudog
- Блокировки горутин (перевод в waiting, gopack, sendq, recvq)
- Оптимизации (sendDirect и т.п.)
- Ссылки:
    - https://www.bestprog.net/ru/2022/02/14/c-ring-queue-developing-a-template-class-that-implements-a-ring-queue-ru/
    - https://www.youtube.com/watch?v=ZTJcaP4G4JM

Результат прохождения блока 4: Студент понимает внутреннее устройство каналов: что конкретно происходит при чтении/записи. 
Эти знания помогут студенту в понимании происходящего в многопоточном приложении, замечать потенциальные аномалии в поведении программы.

### Блок 5. Практические задания
```go
/*
    Вернемся к задаче, которая должна выполняться раз в сутки. 
    Пришли новые вводные, теперь задача должна выполняться, как можно скорее,
    но все еще в фоне.
    Как вы реализуете это?
 */

package worker

// Вызывается при запуске сервиса
func trigger() {
  go worker()
}

func worker() {
  t := time.NewTicker(time.Hour * 24)
  for range t.C {
    executeTask()
  }
}

func executeTask() {
  // execution
}




Решение:

package worker

func startWorker(ch chan any) {
	go worker(ch)
}

func worker(ch chan any) {
	for item := range ch {
		executeTaskForItem(item)
        }    
}

func executeTaskForItem(item any) {
    // execution
}
```

## Урок 3. Примитивы синхронизации
### Блок 1. Мьютексы 
- Определение мьютекса
- sync.Mutex (структура, методы, использование)
- sync.RWmutex (структура, методы, использование)
- Ссылки:
    - https://go.dev/tour/concurrency/9
    - https://pkg.go.dev/sync
    - https://stackoverflow.com/a/19168242/16487299

Результат прохождения блока 1: Студент знает, что такое мьютексы и знаком с их реализацией в Go.
Умеет применять мьютексы для синхронизации. Знает, чем отличаются структуры sync.Mutex и sync.RWMutex, и когда какую из
них лучше применять. На практике эти знания помогут обеспечить потокобезопасность приложения.
### Блок 2. Примеры использования мьютексов

```go
package main

import (
	"fmt"
	"sync"
	//"time"
)

// SafeCounter потокобезопасен.
type SafeCounter struct {
	mu sync.Mutex
	v  map[string]int
}

func (c *SafeCounter) Inc(key string) {
	c.mu.Lock()
	// Lock: только эта горутина может использовать SafeCounter, пока не вызван Unlock.
	c.v[key]++
	c.mu.Unlock()
}


func (c *SafeCounter) Value(key string) int {
	c.mu.Lock()
        // Lock: только эта горутина может использовать SafeCounter, пока не вызван Unlock.
	defer c.mu.Unlock()
	return c.v[key]
}

func main() {
	c := SafeCounter{v: make(map[string]int)}
	for i := 0; i < 1000; i++ {
		go c.Inc("somekey")
	}

	//time.Sleep(time.Second)
	fmt.Println(c.Value("somekey"))
}
```

### Блок 3. sync.WaitGroup
- sync.WaitGroup
- Ссылки:
    - https://metanit.com/go/tutorial/7.7.php
    - https://pkg.go.dev/sync#WaitGroup

Результат прохождения блока 3: Студент знает, что такое структура sync.WaitGroup.
Умеет использовать ее для контроля порядка выполнения операций. На практике эти знания помогут управлять выполнением нескольких горутин.

### Блок 4. Примеры использования sync.WaitGroup

```go
package main

import (
  "fmt"
  "sync"
  "time"
)

func main() { 
    var wg sync.WaitGroup 
    wg.Add(2)       // в группе две горутины
    work := func(id int) { 
        defer wg.Done()
        fmt.Printf("Горутина %d начала выполнение \n", id) 
        time.Sleep(2 * time.Second)
        fmt.Printf("Горутина %d завершила выполнение \n", id) 
   } 
   
   // вызываем горутины
   go work(1) 
   go work(2) 
   
   wg.Wait()        // ожидаем завершения обоих горутин
   fmt.Println("Горутины завершили выполнение") 
}
```

### Блок 5*. sync.Map  
- sync.Map (обзор, методы)
- cache contention как причина использовать sync.Map
- Ссылки:
    - https://pkg.go.dev/sync#Map
    - https://habr.com/ru/articles/338718/

Результат прохождения блока 5: Блок под звездочкой, так как использование sync.Map не самый часто встречающийся кейс. Привычнее и производительнее использовать
обычную map в связке с sync.Mutex\RWMutex для обеспечения безопасного доступа к map. Однако в некоторых ситуациях (обычно при терминальных нагрузках) у этого решения могут возникать проблемы. 
В таких случаях sync.Map может стать решением.
### Блок 6. Практические задания
```go
/*
    У вас есть метод, в котором выполняется 3 запроса в другие сервисы.
    По трассировке вы видите, что эти 3 запроса выполняются 
    последовательно, и каждый занимает примерно по 2 секунды. 
    Поэтому работа вашего метода занимает более 6 секунд.
    Что бы вы предприняли, чтобы ускорить запрос? Учитывайте, что данные из 
    запросов в другие сервисы используются только после того, как все запросы завершены. 
    Ответ первого запроса не используется в теле второго запроса и т.д.
 */

// ...
package handlers

func heavyHandler() (Response, error) {
        dataFromServiceOne, err := callServiceOne()
        if err != nil {
            // handle error
        }
        dataFromServiceTwo, err := callServiceTwo()
        if err != nil {
            // handle error
        }
        dataFromServiceThree, err := callServiceThree()
        if err != nil {
            // handle error
        }
		
		
	return buildResponse(dataFromServiceOne, dataFromServiceTwo, dataFromServiceThree), nil
}
// ...


// Решение:

func heavyHandler() (Response, error) {
    wg := sync.WaitGroup{}
    wg.Add(3)
    var errorInParallelCalculations error
	data := make(map[int]any, 3)
    go func() {
      defer wg.Done()
      dataFromServiceOne, err := callServiceOne()
      if err != nil {
        errorInParallelCalculations = err
      }
      data[1] = dataFromServiceOne 
    }()
    go func() {
      defer wg.Done()
      dataFromServiceTwo, err := callServiceTwo()
      if err != nil {
        errorInParallelCalculations = err
      }
      data[2] = dataFromServiceTwo
    }()
    go func() {
      defer wg.Done()
      dataFromServiceThree, err := callServiceThree()
      if err != nil {
        errorInParallelCalculations = err
      }
      data[3] = dataFromServiceThree
    }()
	
    wg.Wait()  
  
    return buildResponse(data), nil
}

```
